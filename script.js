var gs = {};
gs.name;
gs.loaded = false;
gs.images = [];
gs.msgNo = 0;
gs.msg = "";

/**************** DOM and functions loaded on startup ****************/

document.addEventListener("DOMContentLoaded", function(event)
{
    gs.loaded = true;
    init();
} );

function init()
{
    if (gs.loaded)
        {
            cacheImages();
            customEventListenerCreation();
        }
}

function cacheImages()
{
    var images = document.getElementsByTagName("img");
    for (var i = 0; i < images.length; i++)
    {
        gs.images[i] = images[i];
    }
}



/**************** Event creation & handlers ****************/

function customEventListenerCreation()
{
    var nameButton = document.getElementById("forum_submit");
    nameButton.addEventListener("click", login);
    var sendButton = document.getElementById("send_message");
    sendButton.addEventListener("click", sendMessageByClick);
    var enterMessage = document.getElementById("textToSend");
    enterMessage = addEventListener("keyup", sendMessageByEnter);
}

function login()
{
    // No longer works as the college discontinued feature
    alert('ERROR: This feature is no longer supported!');
    /*
    gs.name = document.getElementById("name").value;
    if (gs.name == "")
    {
        alert('Cannot login without a username!');
    }
    else if (gs.name.length < 3)
    {
        alert('A username must be at least 3 characters');
    }
    else
    {
        getWelcomeMessages();
    }*/
}

function sendMessageByClick()
{
    var userMsg = document.getElementById("message_input").value;
    document.getElementById("message_input").value = null;
    if (userMsg.length < 1) // If the message is blank, do not make request
    {
        alert('Please enter a message');
    }
    else
    {
        var welcomeRequest = { msgNo: gs.msgNo + 1, username: gs.name, msg: userMsg };
        $.ajax({url: "chatpgm.php", type: "POST", async: false, data: welcomeRequest,
        success: function(result)
        {
            var messageXML = stringToXML(result);
            var messages = messageXML.getElementsByTagName("message");
            for (var i = 0; i < messages.length; i++)
            {
                if (messages[i].getElementsByTagName("msgNo")[0].textContent == gs.msgNo)
                {
                    addToMessages(messages[i]);
                }
            }
        },
        error: connectionError});
    }
}

function sendMessageByEnter()
{
    event.preventDefault();
    if (event.keyCode === 13)
    {
        document.getElementById("send_message").click();
    }
}



/**************** Helper methods ****************/

function organizeChatroom()
{
    document.getElementById("name").textContent = "";
    document.getElementById("name").style.visibility = "hidden";
    document.getElementById("forum_submit").style.visibility = "hidden";
    document.getElementById("send_message").style.visibility = "visible";
    document.getElementById("message_input").placeholder = "Enter a message here...";
    document.getElementById("message_input").disabled = false;
    document.getElementsByClassName("container")[0].style.marginTop = "-40px";
}

function getWelcomeMessages()
{
    var welcomeRequest = { msgNo: gs.msgNo, username: gs.name, msg: "none" };
    $.ajax({url: "chatpgm.php", type: "POST", async: false, data: welcomeRequest,
    success: function(result)
    {
        alert('Successfully logged in as ' + gs.name + '!');
        organizeChatroom();

        var messageXML = stringToXML(result);
        var messages = messageXML.getElementsByTagName("message");
        addToMessages(messages[0]);
        addToMessages(messages[1]);
        addToMessages(messages[2]);

        gs.msgNo = 3;

        checkIfNewMessages();
    },
    error: connectionError});
}

function stringToXML(stringToBeXML)
{
    return (new DOMParser()).parseFromString(stringToBeXML, "text/xml");
}

function connectionError(jqXHR, textStatus, errorThrown)
{
    alert('An error occurred... Look at the console (F12 or Ctrl+Shift+I, Console tab) for more information!');

    $('#result').html('<p>status code: '+jqXHR.status+'</p><p>errorThrown: ' + errorThrown + '</p><p>jqXHR.responseText:</p><div>'+jqXHR.responseText + '</div>');
    console.log('jqXHR:');
    console.log(jqXHR);
    console.log('textStatus:');
    console.log(textStatus);
    console.log('errorThrown:');
    console.log(errorThrown);
}

function addToMessages(message)
{
    var username = message.getElementsByTagName("username")[0].textContent;
    var msg = message.getElementsByTagName("msg")[0].textContent;

    var messageAsString = username + ": " + msg;
    var p = document.createElement("p");
    p.innerHTML = messageAsString;

    document.getElementById("messages").appendChild(p);
}

function checkIfNewMessages()
{
    var welcomeRequest = { msgNo: gs.msgNo, username: gs.name, msg: "none" };
    $.ajax({url: "chatpgm.php", type: "POST", async: false, data: welcomeRequest,
    success: function(result)
    {
        var messageXML = stringToXML(result);
        var messages = messageXML.getElementsByTagName("message");

        for (var i = 0; i < messages.length; i++)
        {
            addToMessages(messages[i]);
        }

        if (messages.length > 0)
        {
            document.getElementById("messages").scrollTop = document.getElementById("messages").scrollHeight;
        }
        gs.msgNo = messages[messages.length - 1].getElementsByTagName("msgNo")[0].textContent;
    },
    error: connectionError});
    setTimeout(checkIfNewMessages, 1000);
}