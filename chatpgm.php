<?php
	// Declare necessary connection variables and database identifiers
	$host = 'localhost';
	$user = 'chatpgm';
	$passwd='V9p5r';
	$database = 'ia2chat';
	$table = 'messages';

	//Get variables sent from client
	$msgNo = $_POST['msgNo'];
	$username = $_POST['username'];
	$msg = $_POST['msg'];
    
	$connection =  mysqli_connect($host, $user, $passwd, $database);

    
    if (mysqli_connect_errno()) {
		die("Connection failed ho : " . mysqli_connect_error());
    } 
    
	
	//Test for type of data received
	if ($msgNo == 0){				//Chatbox is being activated
		getWelcome($connection);
	}
	elseif ($msg == "none"){		//Periodic retrieval with no send
		getNewmessages($connection);
	}
	else {							//Message received
		storeMessage($connection, $username, $msg);
		getNewMessages($connection);
	}

	$connection->close();
	
	//Used to determine what the last record in the database is currently
	function getLast($connection){
		$SQLcmd = "SELECT msgNo FROM messages ORDER BY msgNo DESC LIMIT 1";
		$results = $connection->query($SQLcmd);
		
		
		if ($results->num_rows > 0){
			while ($row = $results->fetch_assoc()){
				$last = $row['msgNo'];
			}
			return $last;
		}
	}	
	
	//Run when chat room is to be activated
	function getWelcome($connection){
       
		$last = 3; //there are 3 welcome messages?	
		$SQLcmd = "SELECT * FROM messages ORDER BY msgNo LIMIT 3"; //get the first 3 messages
        
		getData($SQLcmd, $connection, $last);	//send query to work engine 
	}

	//Run when server is polled for new messages
	function getNewMessages($connection){
        $last = getLast($connection);		//determine current last record
        $SQLcmd = "SELECT * FROM {$GLOBALS['table']} WHERE msgNo > {$GLOBALS['msgNo']}";	//prepare sql query for message retrieval
        getData($SQLcmd, $connection, $last);				//send query to work engine
	}

	//Work engine
	function getData($SQLcmd, $connection, $last){	

		$results = $connection->query($SQLcmd);	//make query that was passed to work engine
			
		
		if (!$results->num_rows >0){			// test if query succeded or not
			echo "unable to execute the query"; // this will happen often
		}
		else {
			echo "<chat id = '$last'>";	//build data file of new messsages to be returned to client
			while ($row = $results->fetch_assoc()){	
				echo "<message>";
				echo "<msgNo>".$row['msgNo']."</msgNo>";
				echo "<username>".$row['username']."</username>";
				echo "<msg>".$row['msg']."</msg>";
				echo "</message>";
			}
			echo "</chat>";
		}

	}

	//Run when user send a new message to chat room 
	function storeMessage($connection, $username, $msg){
		$SQLcmd = "INSERT INTO messages (username, msg) VALUES ('".$username."', '".$msg."')"; //prepare query string to insert data 
		$results = $connection->query($SQLcmd);				//make query to insert data 
		if (!$results){										//test if insertion was a success
			die('Error : ('. $connection->errno .') '. $connection->error);	 // display criptic error message if unsuccessful - helps find problems
		}
	}
	
?>

