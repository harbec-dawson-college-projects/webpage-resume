var globalVars = {};
globalVars.loaded = false;
globalVars.tiles = [];
globalVars.imageArray = [];
globalVars.backgrounds = [];
globalVars.imageIds = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p'];
globalVars.tileIds = ['tilea', 'tileb', 'tilec', 'tiled', 'tilee', 'tilef', 'tileg', 'tileh', 'tilei', 'tilej', 'tilek', 'tilel', 'tilem', 'tilen', 'tileo', 'tilep'];
globalVars.isUsed = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
globalVars.tileLetter = [65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80];
globalVars.isFlipped = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
globalVars.backgroundCount = 0;
globalVars.clickCount = 0;
globalVars.cover1;
globalVars.cover2;
globalVars.coverId1 = "";
globalVars.coverId2 = "";
globalVars.imgPath1 = "";
globalVars.imgPath2 = "";
globalVars.matchCount = 0;

document.addEventListener("DOMContentLoaded", function(event)
{
    globalVars.loaded = true;
    init();
} );

function init()
{
    if (globalVars.loaded)
        {
            cacheCovers();
            cacheImages();
            cacheBackgrounds();
            addEventListeners();
        }
}

/* Hides all cover tiles */
function hideAllCovers()
{
    for (var i = 0; i < 16; i++)
        {
            document.getElementById(globalVars.tileIds[i]).style.visibility = 'hidden';
        }
}

/* Caches cover images */
function cacheCovers()
{
    globalVars.tiles = ['images/tile_letters/a.jpg', 'images/tile_letters/b.jpg', 'images/tile_letters/c.jpg',
                       'images/tile_letters/d.jpg', 'images/tile_letters/e.jpg', 'images/tile_letters/f.jpg',
                       'images/tile_letters/g.jpg', 'images/tile_letters/h.jpg', 'images/tile_letters/i.jpg',
                       'images/tile_letters/j.jpg', 'images/tile_letters/k.jpg', 'images/tile_letters/l.jpg',
                       'images/tile_letters/m.jpg', 'images/tile_letters/n.jpg', 'images/tile_letters/o.jpg',
                       'images/tile_letters/p.jpg'];
}

/* Caches character images */
function cacheImages()
{
    globalVars.imageArray = ['images/tiles/aang.jpg', 'images/tiles/azula.jpg', 'images/tiles/katara.jpg',
                             'images/tiles/mai.jpg', 'images/tiles/sokka.jpg', 'images/tiles/toph.jpg',
                             'images/tiles/ty_lee.jpg', 'images/tiles/zuko.jpg', 'images/tiles/aang.jpg',
                             'images/tiles/azula.jpg', 'images/tiles/katara.jpg', 'images/tiles/mai.jpg',
                             'images/tiles/sokka.jpg', 'images/tiles/toph.jpg', 'images/tiles/ty_lee.jpg',
                             'images/tiles/zuko.jpg'];
    
    randomizeImages();
}

/* Caches background images */
function cacheBackgrounds()
{
    globalVars.backgrounds = ['images/backgrounds/background.jpg', 'images/backgrounds/background2.jpg', 'images/backgrounds/background3.jpg', 'images/backgrounds/background4.jpg', 'images/backgrounds/background5.jpg'];
    
    setBackground();
}

/* Used to randomize the images for that round of the game */
function randomizeImages()
{
    var randPos = getRandomInt(16);
    
    // Will randomly place images
    for (var i = 0; i < 16; i++)
        {
            while (globalVars.isUsed[randPos] == 1)
                {
                    randPos = getRandomInt(16);
                }
            
            document.getElementById(globalVars.imageIds[i]).src = globalVars.imageArray[randPos];
            globalVars.isUsed[randPos] = 1;
            
            // New random variable for the next iteration
            randPos = getRandomInt(16);
        }
}

/* Changes or sets the background image */
function setBackground()
{
    if (globalVars.backgroundCount == 0) document.getElementById("bg").src = globalVars.backgrounds[0];
    if (globalVars.backgroundCount == 1) document.getElementById("bg").src = globalVars.backgrounds[1];
    if (globalVars.backgroundCount == 2) document.getElementById("bg").src = globalVars.backgrounds[2];
    if (globalVars.backgroundCount == 3) document.getElementById("bg").src = globalVars.backgrounds[3];
    if (globalVars.backgroundCount == 4)
        {
            document.getElementById("bg").src = globalVars.backgrounds[4];
            globalVars.backgroundCount = 0;
        }
    else
        {
            globalVars.backgroundCount++;
        }
}

/* Creates all event listeners used */
function addEventListeners()
{
    // For clicking the End Game button
    var button = document.getElementById("end");
    button.addEventListener("click", endGame);
    
    // For clicking the Restart button
    var button = document.getElementById("button");
    button.addEventListener("click", newGame);
    
    // For clicking on the cover images
    var covers = document.getElementsByClassName("covers"); // Makes an array of all the divs
    for (var i = 0; i < 16; i++)
        {
            covers[i].addEventListener("click", tile_click);
        }
    
    // For keyboard input
    window.addEventListener("keydown", letter_click);
}

/* Is called on the End Game button press */
function endGame()
{
    var index;
    for (var i = 0; i < 16; i++)
        {
            index = document.getElementById(globalVars.imageIds[i]);
            index.style.visibility = 'hidden';
            
            index = document.getElementById(globalVars.tileIds[i]);
            index.style.visibility = 'hidden';
        }
}

/* Is called on the Restart button press */
function newGame()
{
    globalVars.isUsed = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    globalVars.isFlipped = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    globalVars.matchCount = 0;
    makeAllCoversVisible();
    randomizeImages();
    setBackground();
    globalVars.clickCount = 0;
}

/* Will hide all tile images and place cover photos on top */
function makeAllCoversVisible()
{
    var index;
    for (var i = 0; i < 16; i++)
        {
            index = document.getElementById(globalVars.imageIds[i]);
            index.style.visibility = 'visible';
            
            index = document.getElementById(globalVars.tileIds[i]);
            index.style.visibility = 'visible';
        }
}

/* The function below is used to get a random int, sourced from the following:
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random*/
function getRandomInt(max)
{
  return Math.floor(Math.random() * Math.floor(max));
}

/* Will be called on any mouse click or keyboard click on a tile */
function tile_click(e)
{
    globalVars.clickCount++;
    
    // Only one image has been clicked
    if (globalVars.clickCount == 1)
        {
            if (e instanceof MouseEvent)
                {
                    var a = document.getElementById(this.id);
                    var index = globalVars.tileIds.indexOf(this.id);
                    if (globalVars.isFlipped[index] == 1)
                        {
                            alert('You cannot select this tile!');
                            globalVars.clickCount--;
                        }
                    else
                        {
                            globalVars.isFlipped[index] = 1; // Indicates the cover tile has been removed
                            a.style.visibility = 'hidden'; // Hides cover layer
                            globalVars.coverId1 = a;
                            globalVars.imgPath1 = document.getElementById(this.id.substr(4)).src; // Gets path of image tile, not cover tile
                            globalVars.imgId1 = this.id; // EX: tilea
                        }
                }
            else if (e instanceof KeyboardEvent)
                {
                    var tileId = globalVars.tileIds[e.keyCode - 65];
                    if (globalVars.isFlipped[e.keyCode - 65] == 1)
                        {
                            alert('You cannot select this tile!');
                            globalVars.clickCount--;
                        }
                    else
                        {
                            globalVars.isFlipped[e.keyCode - 65] = 1; // Indicates the cover tile has been removed
                            var a = document.getElementById(tileId); // This will yield an index in tileIds
                            a.style.visibility = 'hidden'; // Hides cover layer
                            globalVars.coverId1 = a;
                            globalVars.imgPath1 = document.getElementById(tileId.substr(4)).src; // Gets path of image tile, not cover tile
                            globalVars.imgId1 = tileId; // EX: tilea
                        }
                }
        }
    
    // Two images have been clicked
    if (globalVars.clickCount == 2)
        {
            if (e instanceof MouseEvent && globalVars.isFlipped[globalVars.tileIds.indexOf(this.id)] == 1)
                {
                    alert('You cannot select this tile!');
                    globalVars.clickCount--;
                }
            else if (e instanceof KeyboardEvent && globalVars.isFlipped[e.keyCode - 65] == 1)
                {
                    alert('You cannot select this tile!');
                    globalVars.clickCount--;
                }
            else
                {
                    if (e instanceof MouseEvent)
                        {
                            var a = document.getElementById(this.id);
                            var index = globalVars.tileIds.indexOf(this.id);
                            if (globalVars.isFlipped[index] == 1)
                                {
                                    alert('You cannot select this tile!');
                                }
                            else
                                {
                                    globalVars.isFlipped[index] = 1; // Indicates the cover tile has been removed
                                    a.style.visibility = 'hidden'; // Hides cover layer
                                    globalVars.coverId2 = a;
                                    globalVars.imgPath2 = document.getElementById(this.id.substr(4)).src; // Gets path of image tile, not cover tile
                                    globalVars.imgId2 = this.id; // EX: tilea
                                }
                        }
                    else if (e instanceof KeyboardEvent)
                        {
                            var tileId = globalVars.tileIds[e.keyCode - 65];
                            if (globalVars.isFlipped[e.keyCode - 65] == 1)
                                {
                                    alert('You cannot select this tile!');
                                }
                            else
                                {
                                    globalVars.isFlipped[e.keyCode - 65] = 1; // Indicates the cover tile has been removed
                                    var a = document.getElementById(tileId); // This will yield an index in tileIds
                                    a.style.visibility = 'hidden'; // Hides cover layer
                                    globalVars.coverId2 = a;
                                    globalVars.imgPath2 = document.getElementById(tileId.substr(4)).src; // Gets path of image tile, not cover tile
                                    globalVars.imgId2 = tileId; // EX: tilea
                                }
                        }
                    
                    
                    
                    
                    if (tile_match(globalVars.imgPath1, globalVars.imgPath2))
                        {
                            globalVars.imgId1 = globalVars.imgId1.substring(4); // The last letter of a tile cover id name
                            globalVars.imgId2 = globalVars.imgId2.substring(4);

                            var c = document.getElementById(globalVars.imgId1);
                            c.style.visibility = 'hidden';
                            var d = document.getElementById(globalVars.imgId2);
                            d.style.visibility = 'hidden';
                            
                            globalVars.matchCount++;
                            if (globalVars.matchCount == 8)
                                {
                                    alert('   Congratulations! You have won!\n   Press the Restart button to play again.');
                                }
                            globalVars.clickCount = 0; // The images match, clickCount is reset
                        }
                    else
                        {
                            setTimeout(image_flip_back, 1000);
                        }
                }
        }
}

/* Determines if the two paths match */
function tile_match(path1, path2)
{
    if (path1 == path2)
        {
            return true;
        }
    return false;
}

/* Will call the tile_click method if key pressed was valid */
function letter_click(e)
{
    // A = 65
    // B = 66
    // ...
    // P = 80
    if (globalVars.tileLetter.includes(e.keyCode))
        {
            tile_click(e);
        }
}

/* Will be called when the two images do not match */
function image_flip_back()
{
    globalVars.coverId1.style.visibility = 'visible';
    globalVars.coverId2.style.visibility = 'visible';
    
    globalVars.isFlipped[globalVars.tileIds.indexOf(globalVars.imgId1)] = 0;
    globalVars.isFlipped[globalVars.tileIds.indexOf(globalVars.imgId2)] = 0;
    
    globalVars.clickCount = 0;
}


